# Arduino Fish Tank Light Controller

## Purpose

This is a simple sketch designed to control four LEDs for a fish tank.  These
lights are designed to fade in and out, without ever reaching either max or min.
Due to this, your fish tank should illuminate with a pulsating light.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/arduino_fish_tank.git

## Working
![Gamma's fish tank](https://bitbucket.org/mdill/arduino_fish_tank/raw/13a78e87ad9f7b49405cd01ac56599cdc8528d54/blinky.gif)

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/arduino_fish_tank/src/master/LICENSE.txt) file for
details.

