/*
 * 20171229 - Michael Dill
 *
 * This class is to be used to independently control multiple LEDs.  It is
 * recommended that you only use 2-15 LEDs, as it is very "clunky," and if you
 * intend on using more, the TLC5940 would be cleaner to impliment.
 *
 * To use this sketch, you only have access to two modules:
 *      LED objectName( pinNumber );
 *      objectName.ledChange();
 * 
 * These should be self-explanitory:
 *
 * The constructor `LED objectName( pinNumber );` is used to initialized any LED
 * on a specific pin (pinNumber) as an object (objectName).  This will determine
 * a random starting brightness for that LED, pick a random change-rate for that
 * LED, as well as determine if the LED will start by dimming or brightening.
 *
 * The `.ledChange()` definition will update that specific object, randomly.  It
 * will change the brightness, as well as change the change-rate with a 1/25
 * chance.  By calling the object's `.ledChange()` definition, the LED will be
 * automatically refreshed.
 */

// Create our custom LED class
class LED{
    private:
        // Set higest and lowest brightnesses for LEDs
        byte highest = 250,
             lowest = 5;
        byte pin, brightness, isNeg; // Random necessary variables
        int change; // Set the inc/decrement rate for the LED
        int checkNeg(); // See if our change rate is negative, or not
        void changeChange(); // Get a new change rate
    public:
        LED( byte led ); // Define our constructor
        void ledChange ();
};

// Define our constructor
LED::LED( byte led ){
    pin = led;
    changeChange();

    // Set the initial LED brightness by random
    brightness = random( lowest, highest ); 

    // Randomly determine if the LED will start brightening or darkening
    if( random( 1, 2 ) == 1 )
        change *= -1;
}

// Get a new change rate
void LED::changeChange(){
    change = random( 1, 4 );

    if( isNeg )
        change *= -1;
}

int LED::checkNeg(){
    if( change < 0 )
        return 1; // If `change` is negative, return true
    return 0;
}

void LED::ledChange(){
    // Flip LED change value between brightening and darkening
    if( brightness <= lowest || brightness >= highest ){
        change *= -1;

        isNeg = checkNeg(); // Determine if we're brightening/dimming the LEDs

        // A 1-in-25 chance to change the fade-rate
        if( random( 25 ) == 13 )
            changeChange();
    }


    brightness += change; // Adjust the LEDs brightness

    analogWrite( pin, brightness ); // Brighten/darken the LED
}

